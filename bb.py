#!/usr/bin/env python3


from getpass import getuser
import logging
import os
import subprocess
import sys

import typer
from rich.logging import RichHandler


SOURCE_MOUNT = "/mnt/bb"

logging.basicConfig(level="NOTSET", format="%(message)s", datefmt="[%X]", handlers=[RichHandler()])
log = logging.getLogger("rich")


def sh(command: str, check=True, stdout=None):
    result = subprocess.run(command.split(), shell=True, check=check, stdout=stdout)
    return result.stdout.decode('utf-8')


def fill_free_space_with_zeros(source_partition: str):
    if not os.path.isdir(SOURCE_MOUNT):
        os.mkdir(SOURCE_MOUNT)

    if os.listdir(SOURCE_MOUNT):
        raise Exception(f'{SOURCE_MOUNT} not empty!')

    log.info(f'mounting {source_partition} to {SOURCE_MOUNT}')
    sh(f'mount {source_partition} {SOURCE_MOUNT}')

    log.info('filling free space with zeros')
    sh(f'dd if=/dev/zero of={SOURCE_MOUNT}/free_space 2>/dev/null', check=False)

    log.info('removing free space file')
    os.remove(f'{SOURCE_MOUNT}/free_space')

    log.info(f'unmounting {SOURCE_MOUNT}')
    sh(f'umount {SOURCE_MOUNT}')


def dump_compress_omit_zeros(source_device: str, destination_file: str):
    log.info(f'dumping {source_device} to {destination_file}.gz')
    # TODO


def main(source_partition: str, destination_file: str):
    source_device = sh(f'/dev/$(lsblk -no PKNAME {source_partition})', stdout=subprocess.PIPE)

    if 'linux' in sys.platform:
        if getuser() != 'root':
            log.fatal("Must be run as root!")

    fill_free_space_with_zeros(source_partition)
    dump_compress_omit_zeros(source_device, destination_file)


if __name__ == '__main__':
    typer.run(main)
