#!/usr/bin/env bash


# strict mode
set -euo pipefail
IFS=$'\n\t'

# globals
readonly SOURCE_PARTITION=$1
readonly DESTINATION_FILE=$2
readonly SOURCE_MOUNT="/mnt/bb"
readonly ARGS_COUNT=$#

APP_NAME=$(basename "$0"); readonly APP_NAME
readonly LOG_DIR=/tmp/$APP_NAME
readonly LOG_FILE=$LOG_DIR/$APP_NAME.log

# logging
readonly GREEN='\033[0;32m'
readonly RED='\033[0;31m'
readonly CLEAR='\033[0m'

info() { echo -e "${GREEN}[INFO]    $*${CLEAR}" | tee -a "$LOG_FILE" >&2; }
warning() { echo "[WARNING] $*" | tee -a "$LOG_FILE" >&2; }
error() { echo "${RED}[ERROR]   $*${CLEAR}" | tee -a "$LOG_FILE" >&2; }
fatal() { echo "${RED}[FATAL]   $*${CLEAR}" | tee -a "$LOG_FILE" >&2; exit 1; }

# helper functions
err_report() { error "on line $1"; }
trap 'err_report $LINENO' ERR

continue_on_err() { return 0; }

is_root() { [ "$EUID" -eq 0 ]; }

is_file() {
	local file=$1
	[[ -f $file ]]
}

is_dir() {
	local dir=$1
	[[ -d $dir ]]
}

is_dir_empty() {
	local dir=$1
    [ "$(ls -A "$dir")" ] ||
        true
}

args_count_equals() {
    local expected_args=$1
    [[ $ARGS_COUNT -eq $expected_args ]] 
}

is_dir "$LOG_DIR" || 
    mkdir "$LOG_DIR"


### business logic ###


fill_free_space_with_zeros() {
    is_dir $SOURCE_MOUNT ||
        mkdir $SOURCE_MOUNT

    is_dir_empty $SOURCE_MOUNT ||
        fatal "$SOURCE_MOUNT not empty"

    info "mounting $SOURCE_PARTITION to $SOURCE_MOUNT"
    mount "$SOURCE_PARTITION" $SOURCE_MOUNT

    info "filling free space with zeros"
    dd status=progress if=/dev/zero of=$SOURCE_MOUNT/free_space 2>/dev/null ||
        continue_on_err
    
    info "removing free space file"
    rm $SOURCE_MOUNT/free_space

    info "unmounting $SOURCE_MOUNT"
    umount $SOURCE_MOUNT
}

dump_compress_source_device_omit_zeros() {
    SOURCE_DEVICE=/dev/$(lsblk -no PKNAME "$SOURCE_PARTITION"); readonly SOURCE_DEVICE

    info "dumping $SOURCE_DEVICE to $DESTINATION_FILE.gz"
    dd status=progress conv=sparse if="$SOURCE_DEVICE" | 
        gzip > "$DESTINATION_FILE".gz
}

main() {
	is_root ||
		fatal "Must be run as ROOT"

    args_count_equals 2 ||
        fatal "usage: bb SOURCE_PARTITION DESTINATION_FILE"

    fill_free_space_with_zeros "$SOURCE_PARTITION"
    dump_compress_source_device_omit_zeros "$DESTINATION_FILE"
}
main
